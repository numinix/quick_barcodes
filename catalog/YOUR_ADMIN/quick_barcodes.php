<?php
/**
 * @package admin
 * @copyright Copyright 2003-2010 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: attributes_controller.php 2 2010-10-17 05:06:08Z numinix $
 */
require('includes/application_top.php');
$post_keywords = zen_db_prepare_input($_POST['keywords']);

if (MODULE_BARCODE_SWITCH == 'true') {
    include(DIR_WS_CLASSES . 'numinix_barcodes.php');
    $numinix_barcodes = new numinix_barcodes();
}

function barcode_block($barcode){
      // BEGIN BARCODE           
      if (MODULE_BARCODE_SWITCH == 'true' && $barcode != '') {
      
          if (MODULE_BARCODE_MODE == 'image') {
              echo "<img align='right' src='qrcode.php?code=".$barcode."' width='70' height='70'>";
           } else {
            $bars = barcode_encode($barcode,'ANY');
            echo 'output'.barcode_outhtml($bars['text'], $bars['bars'], 1, 0, '');  
          }
        } else {
          echo '';
        }
      // END BARCODE 
  }

switch ($_REQUEST['action']) {
    case 'search':
        if (isset($post_keywords) && $post_keywords != '') {
            $where = ' WHERE pd.products_name LIKE "%' . $post_keywords . '%" or products_model LIKE "%' . $post_keywords . '%" or products_id=' . (int) $post_keywords . '';
        }
    // no break
    default:
        $live = false;
        $prods = array();
        if (QUICK_BARCODES_DISPLAY_INACTIVE == 'false') {
            if ($where == '') {
                $where = " WHERE p.products_status='1' ";
            } else {
                $where .= " and p.products_status='1' ";
            }
        }
        $products_query_raw = "SELECT distinct(p.products_id), pd.products_name, p.products_model, p.products_barcode
                             FROM " . TABLE_PRODUCTS . " p
                             LEFT JOIN " . TABLE_PRODUCTS_DESCRIPTION . " pd ON (pd.products_id = p.products_id)
                             " . $where . "
                             GROUP BY p.products_id
                             ORDER BY pd.products_name ASC";
        $products_split = new splitPageResults($_GET['page'], QUICK_BARCODES_MAX, $products_query_raw, $products_query_numrows);
        $products = $db->Execute($products_query_raw);
        break;
}
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
        <title><?php echo TITLE; ?> - <?php echo BOX_CATALOG_QUICK_BARCODES; ?></title>
        <link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
        <link rel="stylesheet" type="text/css" href="includes/cssjsmenuhover.css" media="all" id="hoverJS">
        <script language="javascript" src="includes/menu.js"></script>
        <script language="javascript" src="includes/general.js"></script>
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="includes/javascript/quick_barcodes.js"></script>
        <script language="javascript"><!--
        function popupWindow(url) {
                window.open(url, 'popupWindow', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width=600,height=460,screenX=150,screenY=150,top=150,left=150')
            }
            //--></script>
        <script type="text/javascript">
            <!--
          function init()
            {
                cssjsmenu('navbar');
                if (document.getElementById)
                {
                    var kill = document.getElementById('hoverJS');
                    kill.disabled = true;
                }
            }
            // -->
        </script>
        <style type="text/css">
            label{display:block;float:left;width:100px;margin-left: 20px;}
            hr{margin: 20px 0 0;}
            h1,h4{margin-left: 20px;}
            h1 {margin: 20px; float: left;}
            .product{float:left;width:100%;}
            .product .inner {padding: 0 20px;}
            .productFields{}
            .productField{float:left;}
            .attribute {float:left;}
            .attribute .inner {padding: 0;}
            .pagination{margin-top: 20px;}
            .clearBoth{clear: both;}
            .searchContainer{padding: 20px; float: right;}
            .searchContainer [type="submit"] {margin: 0;}
            input[type="text"] {float: left;margin-right: 20px;}
            input[type="submit"] {margin: 20px 20px 0;}
            span[id^="barcode"]{float:left;}
            span[id^="use-"]{color:blue;}
        </style>
    </head>
    <body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onload="init()">
        <!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
        <!-- header_eof //-->
        <a href="<?php echo zen_href_link(FILENAME_PRODUCTS_ATTRIBUTES); ?>"><h1><?php echo HEADING_TITLE; ?></h1></a>
        <div class="searchContainer">
<?php echo zen_draw_form('quick_barcodes_search', FILENAME_QUICK_BARCODES, 'action=search', 'post'); ?>
            <input type="text" name="keywords" /><input type="submit" value="Search" />
        </form>
    </div>
    <br class="clearBoth" />
<?php
while (!$products->EOF) {
    ?>
        <div class="product"><div class="inner">
                <h2><?php echo $products->fields['products_name']; ?></h2>
                <h3><?php echo $products->fields['products_model']; ?> (Products ID: <?php echo $products->fields['products_id'];?>)</h3>
                <div class="productField">
    <?php
    $products_attributes = $db->Execute("SELECT pa.products_attributes_id, pa.products_barcode, pov.products_options_values_name 
                                         FROM " . TABLE_PRODUCTS_ATTRIBUTES . " pa
                                         LEFT JOIN " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov ON (pov.products_options_values_id = pa.options_values_id)
                                         WHERE pa.products_id = " . $products->fields['products_id'] . "
                                         AND pa.attributes_display_only = 0
                                         ORDER BY pov.products_options_values_sort_order ASC;");
    $flag_npvim = false;
    $flag_sba = false;
    if ($products_attributes->RecordCount() > 0) {
        if (defined('TABLE_PRODUCTS_VARIANTS_ATTRIBUTES_ATTRIBUTES')) {
            $flag_npvim = true;
        }
        if (defined('TABLE_PRODUCTS_WITH_ATTRIBUTES_STOCK')) {
            $flag_sba = true;
        }
        ?>
                        <h3><?php echo TEXT_PRODUCTS_ATTRIBUTES_ATTRIBUTES; ?></h3>
                        <?php
                        if ($flag_sba == false && $flag_npvim == false) {
                            while (!$products_attributes->EOF) {
                                ?>
                                <h4><?php echo $products_attributes->fields['products_options_values_name']; ?> (Attributes ID: <?php echo $products_attributes->fields['products_attributes_id'];?>)</h4>
                                <div class="productField">
                                    <label><?php echo TEXT_PRODUCTS_BARCODE; ?></label>
                                    <input type="text" name="products_attributes_barcode[<?php echo $products_attributes->fields['products_attributes_id']; ?> ]" value="<?php echo $products_attributes->fields['products_barcode']; ?>" /><br class="clearBoth" />
                                    <?php
                                    if($products_attributes->fields['products_barcode'] == ""){
                                        echo '<span id="use-'.$products_attributes->fields['products_attributes_id'].'" class="products_attributes_barcode">'.$products->fields['products_id'].'-'.$products_attributes->fields['products_attributes_id'].'</span>';
                                    }
                                    ?>
                                </div>
                                <br class="clearBoth" />
                <?php
                $products_attributes->MoveNext();
            }
        } else {
            while (!$products_attributes->EOF) {
                $attributes_name_array[$products_attributes->fields['products_attributes_id']] = $products_attributes->fields['products_options_values_name'];
                $products_attributes->MoveNext();
            }
        }
        if ($flag_npvim == true) {
            $products_attributes_stock = $db->Execute("SELECT * FROM " . TABLE_PRODUCTS_VARIANTS_ATTRIBUTES_STOCK . " WHERE products_id=" . (int) $products->fields['products_id']);
            while (!$products_attributes_stock->EOF) {
                $pas_stock_barcode = $products_attributes_stock->fields['stock_barcode'];
                $stock_id = $products_attributes_stock->fields['stock_id'];
                $paa_fields = $db->Execute("SELECT * FROM " . TABLE_PRODUCTS_VARIANTS_ATTRIBUTES_ATTRIBUTES . " WHERE stock_id=" . (int) $stock_id . " and products_variants_attributes_name='barcode'");
                if ($paa_fields->RecordCount() > 0) {
                    $stock_barcode = $paa_fields->fields['products_variants_attributes_value'];
                } else {
                    $stock_barcode = $pas_stock_barcode;
                }
                $products_attributes_groups = $db->Execute("SELECT * FROM " . TABLE_PRODUCTS_VARIANTS_ATTRIBUTES_GROUPS . " WHERE stock_id=" . (int) $stock_id);
                $products_variants_attr_id = $products_attributes_groups->fields['products_attributes_id'];
                ?>
                                <h4><?php echo $attributes_name_array[$products_variants_attr_id]; ?> (Stock ID: <?php echo $stock_id; ?>)</h4>
                                <div class="productField">
                                    <label><?php echo TEXT_PRODUCTS_BARCODE; ?></label>
                                    <input type="text" id="<?php echo $stock_id; ?>" name="npvim_products_barcode[<?php echo $stock_id; ?>]" value="<?php echo $stock_barcode; ?>" /><br class="clearBoth" />
                                    <?php
                                    if($stock_barcode == ""){
                                        echo '<span id="use-'.$stock_id.'" class="npvim_products_barcode">'.$products->fields['products_id'].'-'.$stock_id.'</span>';
                                    }
                                    ?>
                                    <span id="barcode-<?php echo $stock_id; ?>"><?php barcode_block($stock_barcode); ?></span>
                                </div>
                                <br class="clearBoth" />
                <?php
                $products_attributes_stock->MoveNext();
            }
        } elseif ($flag_sba == true) {
            $products_attributes_stock = $db->Execute("SELECT * FROM " . TABLE_PRODUCTS_VARIANTS_ATTRIBUTES_STOCK . " WHERE products_id=" . (int) $products->fields['products_id']);
            while (!$products_attributes_stock->EOF) {
                $stock_barcode = $products_attributes_stock->fields['stock_barcode'];
                $stock_id = $products_attributes_stock->fields['stock_id'];
                $products_attributes_groups = $db->Execute("SELECT * FROM " . TABLE_PRODUCTS_VARIANTS_ATTRIBUTES_GROUPS . " WHERE stock_id=" . (int) $stock_id);
                $products_variants_attr_id = $products_attributes_groups->fields['products_attributes_id'];
                ?>
                                <h4><?php echo $attributes_name_array[$products_variants_attr_id]; ?> (Stock ID: <?php echo $stock_id; ?>)</h4>
                                <div class="productField">
                                    <label><?php echo TEXT_PRODUCTS_BARCODE; ?></label>
                                    <input type="text" id="<?php echo $stock_id; ?>" name="sba_products_barcode[<?php echo $stock_id; ?>]" value="<?php echo $stock_barcode; ?>" /><br class="clearBoth" />
                                    <?php
                                    if($stock_barcode == ""){
                                        echo '<span id="use-'.$stock_id.'" class="sba_products_barcode">'.$products->fields['products_id'].'-'.$stock_id.'</span>';
                                    }
                                    ?>
                                    <span id="barcode-<?php echo $stock_id; ?>"><?php barcode_block($stock_barcode); ?></span>
                                </div>
                                <br class="clearBoth" />
                <?php
                $products_attributes_stock->MoveNext();
            }
        }
    } else {
        $stock_barcode = $products->fields['products_barcode'];
        ?>
                        <label><?php echo TEXT_PRODUCTS_BARCODE; ?></label>
                        <input type="text" id="<?php echo $products->fields['products_id']; ?>" name="products_barcode[<?php echo $products->fields['products_id']; ?>]" value="<?php echo $stock_barcode; ?>" /><br class="clearBoth" />
                        <?php
                                    if($stock_barcode == ""){
                                        echo '<span id="use-'.$products->fields['products_id'].'" class="products_barcode">'.$products->fields['products_id'].'</span>';
                                    }
                                    ?>
                        <span id="barcode-<?php echo $products->fields['products_id']; ?>"><?php barcode_block($stock_barcode); ?></span>
                        <?php
                    }
                    ?>
                </div></div><br class="clearBoth" />
            <hr />
        </div>
                    <?php
                    $products->MoveNext();
                }
                ?>
<?php
// Split Page
if ($products_query_numrows > 0) {
    ?>
    <div class="smallText pagination" align="center"><?php echo $products_split->display_count($products_query_numrows, PHYSICAL_ATTRIBUTES_MAX, $_GET['page'], TEXT_PRODUCTS_ATTRIBUTES_NUM_PRODUCTS) . '<br>' . $products_split->display_links($products_query_numrows, PHYSICAL_ATTRIBUTES_MAX, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], zen_get_all_get_params(array('page', 'info', 'x', 'y', 'pID'))); ?></div>
    <?php
}
// Split Page
?>
<!-- body_text_eof //-->
<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); 
