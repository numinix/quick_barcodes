<?php

require('includes/application_top.php');

function barcode_block($barcode) {
    // BEGIN BARCODE           
    if (MODULE_BARCODE_SWITCH == 'true' && $barcode != '') {

        if (MODULE_BARCODE_MODE == 'image') {
            echo "<img align='right' src='qrcode.php?code=" . $barcode . "' width='70' height='70'>";
        } else {
            $bars = barcode_encode($barcode, 'ANY');
            echo 'output' . barcode_outhtml($bars['text'], $bars['bars'], 1, 0, '');
        }
    } else {
        echo 'None';
    }
    // END BARCODE 
}

//products
if (isset($_POST['products_barcode'])) {
    foreach ($_POST['products_barcode'] as $key => $val) {
        zen_barcode_save($key, $val, 'product');
        barcode_block($val);
    }
}
//products attributes
if (isset($_POST['products_attributes_barcode'])) {
    foreach ($_POST['products_attributes_barcode'] as $key => $val) {
        zen_barcode_save($key, $val, 'attribute');
        barcode_block($val);
    }
}
//NPVIM
if (isset($_POST['npvim_products_barcode'])) {
    foreach ($_POST['npvim_products_barcode'] as $key => $val) {
        zen_barcode_save($key, $val, 'npvim');
        barcode_block($val);
    }
}
//NPVIM
if (isset($_POST['sba_products_barcode'])) {
    foreach ($_POST['sba_products_barcode'] as $key => $val) {
        zen_barcode_save($key, $val, 'npvim');
        barcode_block($val);
    }
}