<?php


function zen_barcode_save($id, $barcode, $type){
    global $db;
    switch($type){
        case "product":
            $db->Execute("UPDATE ".TABLE_PRODUCTS." SET products_barcode='".$barcode."' WHERE products_id=".(int)$id);
            break;
        case "attribute":
            $db->Execute("UPDATE ".TABLE_PRODUCTS_ATTRIBUTES." SET products_barcode='".$barcode."' WHERE products_attributes_id=".(int)$id);
            break;
        case "npvim":
            $attr_attr = $db->Execute("SELECT * FROM ".TABLE_PRODUCTS_VARIANTS_ATTRIBUTES_ATTRIBUTES." WHERE stock_id=".(int)$id." and products_variants_attributes_name='barcode'");
            if($attr_attr->RecordCount() > 0){
            $db->Execute("UPDATE ".TABLE_PRODUCTS_VARIANTS_ATTRIBUTES_ATTRIBUTES." SET products_variants_attributes_value='".$barcode."' WHERE stock_id=".(int)$id." and products_variants_attributes_name='barcode'");
            }
            else{
                $db->Execute("INSERT INTO ".TABLE_PRODUCTS_VARIANTS_ATTRIBUTES_ATTRIBUTES." ( `stock_id`, `products_variants_attributes_name`, `products_variants_attributes_value`) VALUES (".(int)$id.",'barcode', '".$barcode."')");
            }
        case "sba":
            $db->Execute("UPDATE ".TABLE_PRODUCTS_VARIANTS_ATTRIBUTES_STOCK." SET stock_barcode='".$barcode."' WHERE stock_id=".(int)$id);
            break;
    }
}