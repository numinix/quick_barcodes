<?php
// use $configuration_group_id where needed
$db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES
            ('Max Results Per Page', 'QUICK_BARCODES_MAX', '".MAX_DISPLAY_RESULTS_CATEGORIES."', 'The maximum number of results on a page', " . $configuration_group_id . ", 1, NOW(), NOW(), NULL, '')");

$db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES
            ('Display InActive Products', 'QUICK_BARCODES_DISPLAY_INACTIVE', 'false', 'To show inactive products also select true', " . $configuration_group_id . ", 1, NOW(), NOW(), NULL, 'zen_cfg_select_option(array(\"true\", \"false\"),')");


nmx_check_field(TABLE_PRODUCTS,'products_barcode',true);
nmx_check_field(TABLE_PRODUCTS_ATTRIBUTES,'products_barcode',true);
if (defined('TABLE_PRODUCTS_WITH_ATTRIBUTES_STOCK')) {
    nmx_check_field(TABLE_PRODUCTS_WITH_ATTRIBUTES_STOCK,'stock_barcode',true.'varchar(100) NULL DEFAULT NULL after stock_attributes');
}




// For Admin Pages
$zc150 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= 5));
if ($zc150) { // continue Zen Cart 1.5.0
  // delete configuration menu
  $db->Execute("DELETE FROM ".TABLE_ADMIN_PAGES." WHERE page_key = 'configQuickBarcodes' LIMIT 1;");
  // add configuration menu
  if (!zen_page_key_exists('configQuickBarcodes')) {
    if ((int)$configuration_group_id > 0) {
      zen_register_admin_page('configQuickBarcodes',
                              'BOX_CATALOG_QUICK_BARCODES', 
                              'FILENAME_CONFIGURATION',
                              'gID=' . $configuration_group_id, 
                              'configuration', 
                              'Y',
                              $configuration_group_id);
        
      $messageStack->add('Enabled Quick Barcodes Admin Tool Configuration Menu Item.', 'success');
    }
  }
    // delete configuration menu
  $db->Execute("DELETE FROM ".TABLE_ADMIN_PAGES." WHERE page_key = 'catalogQuickBarcodes' LIMIT 1;");
  // add configuration menu
  if (!zen_page_key_exists('catalogQuickBarcodes')) {
    if ((int)$configuration_group_id > 0) {
      zen_register_admin_page('catalogQuickBarcodes',
                              'BOX_CATALOG_QUICK_BARCODES', 
                              'FILENAME_QUICK_BARCODES',
                              '', 
                              'catalog', 
                              'Y',
                              $configuration_group_id);
        
      $messageStack->add('Enabled Quick Barcodes Admin Tool Catalog Menu Item.', 'success');
    }
  }
}
