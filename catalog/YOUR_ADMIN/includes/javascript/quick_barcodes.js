$(document).ready(function () {
    //show dropdown on click

    function barcodes(stockID, barcode, spanID) {
        $.ajax({
            type: "POST",
            url: 'ajax_quick_barcodes.php',
            data: stockID + '=' + barcode,
            dataType: "html",
            success: function (data) {
                $('span#barcode-' + spanID).html(data);
            }
        });
    }
    // make button changed
    $("input[type='text']").change(function () {
        //console.log('changed  '+ $(this).attr('name')+' to ' + $(this).val());
        var barcode = $(this).val();
        var stockID = $(this).attr("name");
        var spanID = $(this).attr("id");
        if (stockID !== 'keywords') {
            barcodes(stockID, barcode, spanID);
        }
    });
    $("span[id^='use-']").click(function () {
        var spanR = $(this).attr("id");
        var stockIDR = $(this).attr("class");
        var barcode = $(this).text();
        var spanid = spanR.replace('use-', '');
        console.log(barcode);
        $("input[id='"+spanid+"']").val(barcode);
        barcodes(stockIDR+"["+spanid+"]", barcode, spanid);
        $("span#"+spanR).hide();
    });
});

